# Pranav Goel Lab Notebook

## January 20th 2022
### Objective
Iron Out Elements of RFA for approval

### What Was Done
Our initial proposal for RFA had problems TAs seemed concerned about so I decided to talk to some TAs during Office Hours. It seemed like the main issues that needed to be addressed related to having a market and that it was not complex enough. This made it clear that we needed to include a lot more justifications and detailed explanations behind the decisions we made in terms in features. One new thing we considered adding as a result of these conversations was a speaker to make a noise that alerts a user of a change or upcoming tasks.

## January 25th 2022
### Objective
Talk to TA/Professors about issues with RFA

### What Was Done
We talked to several TAs and Professors about our RFA to get a more complete list of issues we need to address. We then regrouped and did the research and discussions necessary to address all of those issues. This turned into an email we all collaborated on which I sent out.

## February 7th - 10th 2022
### Objective
Finish Project Proposal 

### What Was Done
I worked on the visual aid and block diagrams initially. You can see the iteration of completion below for the block diagram:

![Block Diagram V1](./images/Block Diagram V1.png "Block Diagram V1")
This initial design was too complicated with the arrows being hard to follow.

![Block Diagram V2](./images/Block Diagram V2.png "Block Diagram V2")
Although this version was a lot cleaner, there ultimately was too many subsystems and a reorganization was needed. I also needed to make sure that the legend matched. This resulted in the final version:

![Block Diagram V3](./images/Block Diagram V3.png "Block Diagram V3")

I also worked on a visual aid which you can see here:
![Visual Aid](./images/Visual Aid.png "Visual Aid")

Other than that, a lot of time was spent on formatting the entire document, fixing grammar, etc. 

## February 21st - 24th 2022
### Objective
Work on the Design Document

### What Was Done
I added more detailed descriptions for the block diagram, visual aid, and physical Design. I also started researching further into what the relationship between specific environmental factors and productivity. I needed to make sure everything was reputable as this essentially justifies the use of each sensor we decide to use. This also fed into the requirements and verification section of the design document. The research also supported why we plan to use certain values to portray as the ideal values for each environment. For example, we were able to explain why 19°C was the ideal temperature for productivity using a few research papers exploring the topic. Other than that I helped with fixing grammar, formatting, etc. I also helped with the requirements and verification section a little and worked on the bibliography.

## February 21st 2022
### Objective
Design Document Check

### What Was Done
Met with TAs to get feedback on our design document. The main thing was making sure that we can make sure our sensors were accurate enough.

## February 25th - 26th 2022
### Objective
Find all the footprints for each component in the PCB

### What Was Done
I used websites like SnapEDA, Digikey, and Mouser to get specific footprints for each component in the schematic. I also made sure I got CAD files and Symbols as necessary so that they can be used accordingly.

## February 27th - March 1st 2022
### Objective
Finish routing and placing all the components.

### What Was Done
Firstly I had to update the schematic based on the footprints since some of the symbols didn't match the actual components. An example of this would be the rotary encoder.

![Updated Rotary Encoder](./images/Rotary Encoder Update.png "Updated Rotary Encoder")

I then put in the initial layout of the PCB based on the dimensions of the device.

![PCB Layout V1](./images/PCB Layout V1.png "PCB Layout V1")

This was a problem since the PCB firstly had to be 100 x 100mm which this was clearly not. The PCB didn't have to align perfectly with every visual display except a few very specific locations. The MCU also needed to be on the side not the middle as it would be blocking the antenna. All of the components would also be placed ideally based on the orientation of the MCU to eliminate unnecessary crossing as much as possible. After fixing these mistakes I also just did the routing. I also needed to fill in ground planes and added a new component to actually connect the power supply to.

![PCB Layout V2](./images/PCB Layout V2.png "PCB Layout V2")

## March 1st 2022
### Objective
Design Review

### What Was Done
We presented the submitted Design Document to Professor Song. He mainly had concerns about our radar sensors and suggested that we get more concrete data to support the use and viability of the sensor. Luckily, Ben Xie already owned the sensor so we decided to go and test it immediately. We did multiply experiments on the sensitivity of the sensor and found that without any changes it was way too sensitive. It could sense movement from far away, very micro adjustments, and could work through plastic (fairly omnidirectional as well). This of course was too sensitive so we adjusted the resistor values until it matched up with what we were looking for. We still wanted it to be sensitive enough to detect the fine movements. This is because then it can be targeted to someone sitting right in front of it. However we needed the distance away to be a lot closer. After a while we found that the 1.2MOhm Resistor adjusted the sensitivity to be acceptable but have the detection distance to be more ideal.

Another note was that the professor thought the audio system was unnecessary and over complicated the circuit. As we decided it would be a much lower priority aspect of the project since including it doesn't really add to much to the project.

# March 1st - 8th 2022
### Objective
Fix and Finalize PCB Layout

### What Was Done
I first spent making sure there were no more Air Wires which involved adjusting routes and making sure components were grounded correctly. I then spent a lot of time using the DRC to make sure that there wasn't any fundamental problems that needed to be addressed. I then Worked with Ben Xie to verify any issues with the PCB that immediately needed to be fixed. There were a few issues that Ben found I needed to fix but that was quickly resolved. We also decided to switch out the power supply footprint with a connector so that it would be easier to work with. This allowed me to make the PCB smaller! I double checked the PCBWay Design Rules to make sure we weren't making any mistakes. I also double checked if the inductor and capacitor values for buck convertor. This lead me to find some mistakes with the footprint of the inductor and needed to update the PCB as necessary. I found the exact capacitors and values we needed to make it work and updated the footprints accordingly. Thankfully most of the capacitors worked with the generic 0805 size and still had all the same values. The biggest change was finding an inductor that matched our specifications because the one in the data sheet was no longer in production. Thankfully we found something that worked. I also spent quite a bit of time polishing and making things a bit neater. This included rounding the edges of the PCB, adding a Logo, add stitching, fixing the cutouts, and adjusting the routes for better coverage.

![PCB Layout V3](./images/PCB Layout V3.png "PCB Layout V3")

# March 8th 2022
### Objective
Create a BOM and purchase parts

### What Was Done
Worked with Ben Xie and Hongru Wang to make sure that we have the cost breakdown and find all the specific components we need from Digikey and Mouser. We did see that a couple items were out of stock which led to finding alternative components that matched the same footprints and values. Thankfully there were several items that met these constraints.

# March 14th - 20th 2022
Spring Break!

# March 21st 2022
### Objective
Start progress on e-ink display.

### What Was Done
Purchased E-ink Display and started writing code to test the device.

# March 22nd - 28th
### Objective
Start soldering and testing components that have arrived with

### What Was Done
We soldered in all the important parts (buttons, esp32, etc.) and started running continuity checks in order to make sure nothing was broken. We also delivered 5V and 3.3V respectively to components using the power supply and verified that they were functioning as expected. Unfortunately, not all the components we needed arrived during this time frame. We also found that some of the items were either unavailable, discontinued, or not from a vender we can purchase from. To compensate, we searched for alternative components that met the same specifications and were available. Unfortunately the ADP2302ARDZ-5.0 that we wanted was not availble anywhere and there wasn't another footprint we could use because the V2 order was already placed by then.

# March 24th 2022
### Objective
Test E-ink Device

### What Was Done
E-ink display arrived! Started testing with sample code and found that it works fine.
![E-ink Start](./images/E-ink Start.jpg)

# March 29th
### Objective
Develop custom background for E-ink display.

### What Was Done
I spent several hours trying to make a background that could fit the E-ink Screen Size at very precise measurements. I measured the size of the screen and did the math to translate it to pixels on photoshop. However, I could not figure out how to actually get the background to display.

Here is an size comparison for perspective: 

![E-ink Size Comp](./images/E-ink%20Size%20Comp.jpg)

# March 30th - 31st
### Objective
Debug and get E-ink Display to work

### What Was Done
I spent most of the time trying to pair down the e-ink code to what was necessary. At the moment most of my code was completely seperate from the main code because I was first focused on trying to write something that actually worked. I still could not get it to work with my custom design. Eventually I found out that there was an even bigger issue. I found that the code was unable to even communicate with the display, getting "BUSY TIMEOUT" errors. At first I realized it was because the "BUSY" cable was getting low which was causing it to be stuck in a reboot cycle. To remedy this I tried manually sending a signal through using the power supply. This supressed the BUSY TIMEOUT error but did not solve the actual problem. I want to note that coming to this testing required looking in very deep at the [library](https://github.com/ZinggJM/GxEPD2) to see how the E-ink code actually drives the display. I also looked into the [wiki](https://www.waveshare.com/wiki/7.5inch_e-Paper_HAT) to get even more details on how everything worked. Since the problem remained even though the BUSY TIMEOUT error was suppressed, it implied that the issue was related to a connection or something internal. I was afraid something was wrong with the ribbon cable but upon doing continuity checks I found that it was working completely fine. The reason we worried about it was because there was a small bump on the cable.

![E-ink Bump](./images/E-ink%20Bump.jpg)

As such we could only conclude that there was something broken internally that must have happened during transportation.

# April 1st
### Objective
Purchase New E-ink Display

### What Was Done
Instead of wasting time trying to fix something we couldn't, we decided to get another display. Thankfully Amazon has a very generous return policy so we were able to just get it replaced. Now we just had to wait for it to come back before trying again.

# April 4th
### Objective
Obtain Components

### What Was Done
By this point most of our parts had arrived but since we already verified a lot worked and put in the order for our V2 board, we decided to wait before soldering everything in.

# April 5th
### Objective
Get E-ink Display Working

### What Was Done
Our replacement E-ink display arrived! Instead of focusing on the background, this time I first tried to get the main task functionality to work. Since it would be a waste of time to do it seperately with firebase already implemented in our main code, I decided to integrate what I worked on so far with that. This took a bit of time but eventually I wrote something that would theoretically work with our firebase task management system. At this point we were using a manual system of setting tasks, so the current setup would have to be changed later. After sometime, I got the E-ink to display my custom set tasks (not connected to firebase) and used that to measure spacing based on the LED placement. I guessed the offset fairly accurately on the first try (using measurements to help).

![E-ink LED Placement](./images/E-ink%20LED%20Placement.jpg)

I then focused on actually connecting it with firebase. This was a lot harder than initially expected because the firebase c++ library had a lot less functionality than the javascript, python, etc. functionalities. Instead I had to take everything in as a psuedo-JSON which used **pointers to pointers** to replicate the parent - child relationship a JSON file has. As such, I had to use special iterators to go through each possible key and value pair recieved. I do want to note that this was not immediately apparent. At first I spent several hours just trying to make it work with the expected regular method. Unfortunately this was simply not able to connect to the correct firebase. There was also added difficulty in the keys being random character unordered keys which ment I had to be able to extract and order everything without knowing the key. Using this JSON method, however, I was able to simply pull all the tasks in the order of the firebase. So, I tasked hongru with ordering it on the Web App side. In the meantime, I got the firebase functionality to finally work!

![E-ink Firebase](./images/E-ink%20Firebase.jpg)

- Commit: af7dc66a634791baec44fe9251232cbf9bab64d8

# April 6th - 7th
### Objective
Get E-ink Display Background Working

### What Was Done
Since I got the firebase to work I spent this day trying to get the background to work. I found out the reason it wasn't working before was because I got three things wrong. First, I got the wrong pixel type (I was trying vertical pixels when it should be horizontal). Horizontal vs Vertical basically means how it was reading the hex value (left to right vs up and down) when translating it to a bitmap. The second error was that I was trying to convert the image straight to a bitmap. I found out that with photoshop I needed to actually use GIF instead. This actually required a lot of research into how the E-ink Display reads image data. I eventually found a [site](http://javl.github.io/image2cpp/) that parses the GIF file and converts it into a C file that I can use. This worked exceedingly well. Lastly, I realized I set the rotation to horizontal instead of vertical so only part of the image would render. Eventually, after fixing all these mistakes I got the following result.

![E-ink Background 1](./images/E-ink%20Background%201.jpg)
![E-ink Background 2](./images/E-ink%20Background%202.jpg)

- Commit 1: d5bf5e321a9c8669545fdab87ef013f9a9c7c71e
- Commit 2: 7927abb5bf671d261fca388d683bc3bde2b12a56

# April 12th
### Objective
Solder PCB V2

### What Was Done
The V2 PCB arrived so Ben and I started soldering all the components together. This took most of our day. Eventually I found a problem with one of the footprints where it didn't line up with the component.

![led problem](./images/led_problem%201.jpg)
![led problem](./images/led_problem2.png)
![led problem](./images/led_layout.png)

Note that VSS is ground but it was not matching with ground. The solution to this was simply rotating the LED and still soldering it in. This was a difficult task but we managed to make it work.

# April 14th
### Objective
3D print front cover

### What Was Done
I set our 3D cover to start printing.

# April 15th
### Objective
Start firebase task ordering and what sensors should read.

### What Was Done
Worked with Hongru to figure out how we want the data stored in firebase so that it is useful. I also worked a little bit with him to understand how we should be ordering the tasks in the database. Hongru was the one who got all of those two things working on the Web App end.

- Commit: 724d3d6538e1591de51880b50bb556ef6f2ce51d

# April 16th
### Objective
Determine if 3D print works with E-ink display.

### What Was Done
Lined up E-ink display to see if the size fit the display. We found that the front plate was just barely too small initially and needed to be adjusted.

![E-ink Fitting](./images/E-ink%20Fitting.jpg)

# April 18th
### Objective
Implement OTA Updates

### What Was Done
I worked on the portal side of OTA. Essentially I created a portal that you can upload the firmware to. 

This is what the final result looked like:
![OTA Working](./images/firmware_portal.JPG)

In the backend of this website it parses through the file and extracts the version information (a magic string) and then sends both the file and that information to firebase. A big issue I had was getting the the magic string's regex correctly working.

![OTA Problems 1](./images/OTA%20Problems%203.png)
![OTA Problems 1](./images/OTA%20Problems%202.png)
![OTA Problems 1](./images/OTA%20Problems%201.png)

Eventually I got all this setup and testing it with the work Ben did, we verified that everything was up and running!

![OTA Working](./images/OTA%20working.png)

- E-ink fixes commit: fede62364d1c510cbf897756731a82bc36441057
- OTA Update Implemented: 1ea08ff3f30e81b6b5dad2962fe7e0ff492e6587

# April 20th - 21st
### Objective
Implement Environmental Mode on Device

### What Was Done
Implemented first version of Mode. This involved working with all of the sensors and LEDs and basically linking all the information together. I had to do convert the data into "buckets" that represented an LED. Since there was only 14 LEDs to work with I had to pick a range and divide it up by 14. All the values that fell into that point meant that this LED in particular would be illuminated. I wanted certain colors to represent if the value read was ideal or not. So, I also had to add in an ideal range parameter that helped determine the code. Most of the challenge was communicating between several parts of the project to get this to work. Eventually I had a working version. I nice little feature I added was that you can kill the mode by using the knob or if you wait for 2 minutes it will automatically switch back to the task mode.

![mode v2](./images/Mode%20v1.jpg)

- Commit: 6cb9a2b55c97e06285af26a380c1342a32072ab8

# April 21st - 24th
### Objective
Switch to Multicore, 3D Printing, Polish

### What Was Done
I spent most of this time switching over the code so that it works with multicore. I also restructured some of the past work so that it would work better and be more snappy. We got all of this working before the demo day! During this time we were 3D printing fixed front panels and also the background panels (Ben was doing most of this). Eventually we had a working project and just needed to test if it worked with the 12V power supply plug because the adaptors finally came. This seemed to go well without a hitch! However, the next day (April 24th), we found that nothing was working. We found that something had short circuited. After spending a lot of time characterizing the problem we found that none of the issues was involving the 5V and 3.3V Power Rail. This lead us to conclude that something was either wrong with our 12V -> 5V buck converter, or something fried. We took out the buck convertor and found that the problem persisted. We then found out that it was a filtering capacitor that fried. After replacing this, things started working again. However, we found that the rotary encoder was no longer registering. After doing some continuity tests, we found that another filtering capcitor there had fried. After replacing that capacitor again, everything was working perfectly!

Check out the image here:

[Project Image](https://courses.grainger.illinois.edu/ece445/pace/getfile/20433)

- Commit 1: e532eff11cd5ec3143c8d3e9a352b242a538625c
- Commit 2: 74ae956975241e35f6595d0d76c8d7bf0616a1e0
- Commit 3: cda7cafb03419d204f6e0db7dec38e12bb818e2f

# April 25th - May 5th
### Objective
Demo, Presentation, Wrapup

### What Was Done
We demoed our project! It went pretty well and everthing pretty much worked. We also presented and after some feedback we were able to really nail down our presentation skills. I also created a video for EC and the presentation. After this was simple finishing the outstanding assignments!