# Hongru Wang's Notebook

## January 25 2022
### Objective
1. Talk to Professor and TAs in office hour about issues in RFA.
### What Was Done
Feedback from TAs:
>>>
 1. Sending notification to phone is distracting, instead of sending a notification through website, use a band, it vibrates as a notification when urgent task is coming. 
 2. Notification content should be send to the e link display.
 3. Use ECG to detect stress level. 
 4. Use sensor to monitor sleep.
 5. use wifi to connect band and device to the internet, and api.
>>>

## January 30-31 2022
### Objective
1. Work on project proposal
### What Was Done
1. Wrote an design overview for the webapp and server subsystem in high level, including the main purpose of the web app, a description of how the web app will address the user needs and how we defifne the success of the web app.
2. Wrote the Tolereance Analysis section based on the sensitivity issue of RCWL-0516 Doppler Radar Microwave Motion Sensor.
3. Wrote the Ethics and Safety section.

## February 8 2022
### Objective
1. Talk to TA in office hour about issues in Project Proposal.
### What Was Done
Feedback from TA:
>>>
 1. Add citations in project motivation.
 2. For high-level requirements need to have accurate numbers like 90% +- 5%.
 3. Set the same color for the power subsystem in a block diagram, and add labels.
 4. Recommend WROOMfor esp32.
 5. Combine microcontroller and communication to data subsystems; combine UI on the website and UI on the device.
 6. Should specify the frequency for the microcontroller to send data through the wifi module.
 7. Remove setting wifi password parts.
 8. Subsystems requirements should be more precise
 9. Tolerance and analysis need to be mathematical. Look into the part that is most likely to fail and add mathematical analysis. (eg: update the display, how to use motion sensors).
>>>

## February 14 2022
### Objective
1. Set up main page and routing for web app.
2. Add initial task card to task component.
### What Was Done
1. Set up main page and routing for web app.\
![](images/1.PNG)
2. Add initial task card to task component.\
![](images/3.PNG)\
commit:294f87dcf9e561c01933a77c8a79a388ce35ac2b\

## February 21 2022
### Objective
1. Work on design document
### What Was Done
1. Finalized the functional requirements for the web app to be completed, and chose the approriate tech stack for the web app.
2. Construct the initial application architecture.\
![](images/5.PNG)
3. Use formula from datasheet to imporve Tolereance Analysis section.

## February 23-24 2022
### Objective
1. Start to design the schematic for LEDs.
### What Was Done
1. Designed the first version of schematic for LEDs.\
![](images/6.png)

## February 27 2022
### Objective
1. Build add tasks functionality.
### What Was Done
1. Now can add tasks locally.\
![](images/4.PNG)

## March 1 2022
### Objective
1. Modify the schematic of LEDs and start to design PCB for LEDs.
2. Help Ben and Pranav to test RCWL-0516 radar sensor.
### What Was Done
1. Finished the schematic of LEDs and started to design PCB for LEDs.\
![](images/9.PNG)\
![](images/10.PNG)
2. Since we want the RCWL-0516 to detect small movement while keeping a relatively smaller reception limit than 7 m, we tested the sensor and found that setting the R-GN value to 1.2MΩ is optimal to satisfy our requirements because it’s a good balance between the sensitivity to small body movement and reception limits. When R-GN is set to 1.2 MΩ, we found that the motion sensor can detect small movements such as writing with a pen in 5 m range, which is very close to the result obtained from the formula x= ln(1.20.2089)0.5375 = 4.464 m. In addition, movements behind three layers of plastics and lateral movements can be detected with this resistor value. 

## March 3-4 2022
### Objective
1. Build delete tasks functionality.
2. Start to build date time picker functionality.
### What Was Done
1. Now can delete tasks locally.
2. Date time picker added.\
![](images/8.PNG) ![](images/7.PNG)\
commit:5b6b1ba110de771a3ff58f7affe340a4c43c678e\

## March 7 2022
### Objective
1. Finish the PCB design for LEDs.
### What Was Done
1. Remove air wires,changed the trace width to 30 mil, and changed the capacitor to 0.1uF, add outlines.\
![](images/11.PNG) \
![](images/12.PNG)

## March 7-8 2022
### Objective
1. feat:edit a task done; view a task done; 
2. fix: edit date picker not change issue; The date time picker saves date object, while firebase only return strings after the date object is stored, format needs to be converted when saving and retriving due node. 
3. style:change fonts, style cardActions,add hover effects.
### What Was Done
1. Now can edit tasks locally.
2. Supported previewing long tasks through a dialog component.
2. Date time picker works in editing page.\
![](images/13.PNG)\
![](images/31.png)\
commit:b9bf52b46358b89c3c0023681ca90bb16eec208c\
commit:d3d50bd5277e7d97f99f971d4a3b368802db5174\
commit:42a28fc886d0a4bf7855cf1b30ce17207bb243d5\
commit:e09be3ae237e6aaaad735c6339141b5f3ef5dbaa\
commit:25885a6d18817b0a47a729844d51f2a112827add\

## March 11 2022
### Objective
1. Place order on My.ECE.
### What Was Done
1. Order placed.

## March 24-28 2022
### Objective
1. Connect to firebase.
2. Develop tasks APIs.
3. Place another three orders on My.ECE.
### What Was Done
1. feat:Add/edit/delete api created. Now any operations on tasks will be updated in realtime database.\
![](images/14.PNG)\
![](images/15.PNG)
2. Talked to business office and supply center for delayed order.

## March 28-29 2022
### Objective
1. Develop temperature, humidity, AQI components.
2. Develop environment data APIs.
### What Was Done
1. feat:envirment page initial version completed, backgounrd changes when certain threshholds are met.
2. feat:temperature, humidity, AQI APIs created, now any modification on the values will be reflected in realtime database.\
![](images/16.PNG)\
![](images/28.png)\
commit:911582a8aebac883c82130dedd8cc5079c0af90a\
commit:019f31743804489d0d1a137495d245afa0dd1cbe\

## March 30 2022
### Objective
1. Develop notification for long time sitting.
### What Was Done
1. feat: Notifications of long sitting time warnings on the web application have been implemented and tested.\
![](images/17.PNG)\
commit:8538aeca5367d39b7d9562a44244b91e027ef2b5\

## April 4 2022
### Objective
1. Allow the user to mark tasks as complete.
### What Was Done
1. feat:add task status;fix:move to da6d directory.
2. feat:enable checking complete and total count in db, will be used in circular progress bar.
3. feat:add more tooltips.\
commit:e20f99f81fc7f58263f0851cef695b5b933d10ed\
commit:f6b93bc4d1a253817936078520f9cee97a1ff74d\
commit:f0c4cf1760294e8a5523fd0fccd493e1b05c865e\

## April 6 2022
### Objective
1. Have a progress component to allow the user to meature the productivity in dashboard page.
### What Was Done
1. feat: built a pie chart to show hom many tasks are inprogress and how many are completed.\
![](images/18.PNG)\
commit:a3ef1d1aa91d297c96eb82531b8ade7e4e45e4ac\

## April 12 2022
### Objective
1. Imrpove the temperature, humidity, AQI components by adding animations.
### What Was Done
1. feat:add animations and interactions with temperature,humidity,and AQI cards.\
![](images/19.png) ![](images/20.png) ![](images/21.png)\
commit:f5ece1f8f7f0b74d9a3b5f24ce2b4be805795e30\


## April 13-15 2022
### Objective
1. Learn how to develop chrome extension, and started to develop our own chrome extension.
### What Was Done
1. feat: built the chrome extension based on a template, but needed to be migrated to manifest V3.
2. feat: chrome extension functionality worked and now can support use short cut to trigger the extension and add tasks through th exension.
3. feat: chrome extension connected to firebase real time database.
4. style: needed more styling.\
![](images/22.PNG)\
commit:0ffd78224a4b70413f69f1a272ad00f7c473241b\
commit:f80d65ab3e91bfaab4414b193c1b4282ad79273b\
commit:5d994e012cee83cffff0360671f36dd75f0e0001\
commit:9aa7c44455f20a4e40a8b986708c49304480c4d5\

## April 15-16 2022
### Objective
1. Sort the data in database without update all the data in realtime database.
2. Enable user to select due time in chrome extension.
### What Was Done
1. feat: set priority in firebase done
2. fix: sort data in database and frontend based on priorit note without updating all data.\
![](images/23.PNG)\
![](images/32.png)
3. feat:add datetimepicker to chrome extension.\
![](images/24.PNG)\
commit:2507d5d971f61bd01248fc1f84722a0c3905d023\
commit:4862b60610aa8224413519771d3dba873f3b8657\

## April 18 2022
### Objective
1. Host the web app instead of run it locoally.
### What Was Done
1. feat: host the web app on firebase. https://esp32-firebase-test-78ff0.web.app/
2. fix: update task status node each time task information changes from web app and device;delete task body for better UX;\
![](images/30.png)\
commit:048ffa336321e8b03378e4921e12e7f686953ea6\
commit:41ad489d3c53b77bd3d6b5443eb2cd210fd3f2d6\

## April 19 2022
### Objective
1. Migrate chrome extension manifest from v2 to v3.
2. Publish the chrome extension on chrome web store.
### What Was Done
1. fix: migrate chrome extension manifest from v2 to v3.
2. fix: submit publish request to chrome web store.\
![](images/26.PNG)
3. fix: delete value node in AQI/TEMP/HUM node.
4. fix: resolve save unchanged date object in udpate task page. bug:when update the task, if the due date does not change, the date object cannot be saved in realtime database.\
commit:3321ca4e192617de47b8c79b65ee4a2798c750a5\
commit:8b111cc36259af59a79370593c7bbf58c0bf0f68\
commit:f5fac022994569642bdd6ad471b03a28e40fef08\\

## April 20 2022
### Objective
1. Build subsitution for react-environment-chart because it cannot be customized.
### What Was Done
1. feat: humidity component now support customized ideal humidity range by controlling the slider;
2. feat: new AQI component done based on react-chartjs-2.
3. fix: environment page finalized.\
![](images/25.PNG)
4. fix: "status:706431100" bug\
commit:edc7cd9c353ee7f81b618a60e357f65d79202608\
commit:b0e0cf0c781d937051c6241ad83f2087b3c8339d\

## April 21 2022
### Objective
1. Mock Demo with Pooja.
### What Was Done
1. Feedback from Pooja:
>>>
1. Put circuit in cover when demo.
>>>

## April 23-25 2022
### Objective
1. Build the logic to calculate the time the user works to the time the user sit in front of the device.
2. Send notification when the user has been sitting for more than 60 mins.
3. Demo with Prof.Song and TAs.
### What Was Done
1. feat: support checking how long the user has been working on frontend.
2. fix: notification works on web app.
3. fix:updade update node when 'dismiss' is clicked; fix: update update node when task is updated.\
![](images/27.PNG)\
![](images/29.png)\
commit:306675f3801f02daf61aae36f0249074a7ad0cd2\
commit:b137de335a45bc0ca42a9e8b6e64f1ede87fe7f2\
commit:52e5f25378f7a7ad7ab80d9a90d4e4d49343d73d\
commit:d3b6c9bb1cd2805638e9eeee50aabfb65499a06d\

## April 27-28 2022
### Objective
1. Prepare slides for Mock Presentation.
2. Mock Presentation with TAs.
### What Was Done
1. Prepared slides with Ben and Pranav. Worked on "future work", "what was done", "look into data", and "software requirment met" sections.
2. Feedback from TAs.
>>>
1. Should not present What was done slide.
2. Change table format in requirement met slide.
3. Have a larger block diagram.
>>>

## May 1-2 2022
### Objective
1. Prepare slides for Presentation.
2. Help Pranv with making film for extra credit.
3. Presentation with Prof.Song and TAs.
### What Was Done
1. Prepared slides, and rehersed with Ben and Pranav.
2. Helped Pranv with making film for extra credit.
3. Feedback from Prof.Song and TAs.
>>>
1. Continue working on the project in the future.
>>>

## May 3-5 2022
### Objective
1. Write Final Report.
### What Was Done
1. Worked on Cost, Verification of Software Subsystem, Design of Software Subsystem, and Future Work Sections.

