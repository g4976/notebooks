# Ben Xie's Lab Notebook

## January 25 2022
### Objective:
The goal for today was to talk with the TA's and figure out what was wrong with our RFA and fix it
### What was done:
Talked with TAs and Professors after class and obtained of list of potential issues. After class we met again as a group and researched the issues, and found ways to address them. We then sent an email to the professors requesting for a reassesment of our RFA.

## January 30/31 2022
### Objective:
Start the formal Project Proposal
### What was done
I started the project proposal and created the title page, as well as drafting and writing the Problem, Solution, High Level Requriements sections, as well as the subsystem overview for the Power, UI, and Sensing subsystems. Also helped fix the system block diagram. In the process of writing the subsystem overview and checking the block diagram, I found potential sensors and power supplies that could be used.

## February 11/12 2022
### Objective:
Create a preliminary BOM for the major components of the project
### What was done:
I needed to find parts for all of our subsystems. I started with the macro parts: The E-ink display, PLA Filament to 3d print the shell of our device, a 120VAC to 12VDC power supply, and the RCWL-0516 radar module. These were pretty self explanitory, and we had already found suitable parts while writing our project proposal.

The next thing I found was the microcontroller. We had already decided to go with the ESP-32, but I narrowed it down to the ESP32-S2-SOLO-N4R2 module for our project.

For the sensing subsystem, we had already decided on the SGP40 for eCO2 and the SHT40 for temperature and humidity. It was pretty simple to find the exact part numbers on mouser/digikey. I chose the SFH3711 for sensing ambient light.

Next up was the UI subsystem. First up was finding a rotary encoder with button. There were many options, mostly differing in stem size and PPR. I ended up choosing the PEC11R-4115F-S0018 on digikey, although that is likely to change as we start on the mechanical design of our project. The PCB footprint should be consistent for all related encoders, so switching this part should not be difficult. We also needed buttons to control the boot/flash of the microcontroller, as well as for the UI. I semi-randomly chose tactile switch, because any switch should work. Finally, I had to choose the RGB LEDs. I chose the NEOPIXEL MINI 3.5mm by 3.5mm LEDs for that, because they are individually addressable which will free up GPIOs on our microcontroller.

The speaker needed an audio op-amp. I chose the LM386MMX-1/NOPB because it was well documented, and fell within our design requirements in terms of maximum power.

After all of these devices were chosen, all that remained was the power subsystem. For this, I added up the manufacturer specified maximum current for all the previous devices, and added an ~25% safety margin. We needed both 5V and 3V3 on our board, so I chose to use a buck converter to drop the 12V input to 5V, and then a linear LDO regulator to convert 5V to 3V3. This was done to minimize power loss, as using a linear converter from 12V to 5V or 3V3 will result in alot of power wasted.

### Link to document: 
https://docs.google.com/spreadsheets/d/11UYQPU8ys0lBJVaoDbZlMoELnDS2-qbKMWt_c10p2is/edit#gid=0


## February 15 2022
### Objective:
The goal for today is to create the first draft of our PCB schematic

### Resources:
https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32e_esp32-wroom-32ue_datasheet_en.pdf
https://dl.espressif.com/dl/schematics/ESP32-Core-Board-V2_sch.pdf

### What was done:
When starting the design, I realized that I had previously chosen the new version of the ESP32 (V2). Because this may lead to compatability issues with different libraries down the line, I ended up switching to the ESP32-WROOM-32E.

The first thing I did was add in the ESP-32 symbol, and add the 3V3, GND, JTAG, and UART connections. I then added buttons with pull-up resistors and debouncing circuits for EN, BOOT, and RESET. The first two are for flashing and rebooting the ESP-32, and the last is so we can reset the saved Wi-Fi password. UART is needed to program the microcontroller, and JTAG can be useful for debugging, although it isn't required. I added a single .1uF capacitor near the VDD input. The value for this decoupling capacitor was different on the two deisgn documents I linked, so I just chose one of them.

![image](images/sch_esp32_buttons_UART_JTAG.JPG)

Next, I worked on the power subsystem. I ended up choosing a new LDO regulator simply because it had the same characteristics and already existed within KiCAD's libraries. The boost converter was probably the most difficult part to design, since it requries a specially chosen capacitor and inductor to operate. I got the required values from the device's datasheet. I also added 22uF bulk capacitors to both the 5V and 3V3 power rails. I also added LEDs to indicate if the 12V, 5V, and 3V3 rails were working.

![image](images/power.JPG)

Next up was the sensing subsystem. Because both the SGP40 eCO2 sensor and SHT40 temp/humidity sensor used I2C, these were pretty simple to implement. I used the generic 10KOhm value for the pull up resistors on the SDA and SCL lines, although we might want to calculate better values down the line. I used the reccommended filtering capacitor values for VDD.

The phototransistor was pretty simple to design, it is a well-documented component, simply using a current sense resistor to detect the current pushed by the transistor. This connected to an ADC pin on the ESP32. The Radar module is premade, so all I had to do was add a connector that it can attach to.

![image](images/Sensing.JPG)

The UI subsystem was pretty simple as well. The rotary encoder had 3 pins for the encoder and 2 for the integrated switch. Debouncing circuits for both were somewhat complex but very well documented. The button design could be copied straight from the buttons outlined above. The eink display only required a connector that had SPI's MOSI, SCK, and CS, as well as DC, RST, and BUSY which are specific to eink displays. The RGB LEDs were also pretty simple. We needed 3 LEDs on the board isself, and a connector to connect the rest of them to the PCB. Each one also needed a decoupling capacitor for VDD. A resistor in series with the data input helps eliminate signal reflections and other wierd phenomena that can cause the LEDs to glitch.

![image](images/UI.JPG)

Last up was the speaker amplifier. This design was taken directly from the device's datasheet, although we might want to verify the gain values and add a way to tweak it after getting the boards manuactured.

![image](images/Speaker_2.JPG)

### Relavant Commits
8323903d98e1e1d3b0388c1373191a8eef487644
b48386611ebc2c230e7096674c6cebdb4030fc8f
c201c0371cf0dfd19188419c55febf104d0380f6 

### Todo:
1. Check bulk/filtering capacitor values
2. Check EN pin delay
3. Check speaker gain, add places for resistors on gain selection

## February 16 2022
### Objective:
The goal for today is to get basic firebase/wifi functionality working on an ESP32 dev-board

### Resources:
I used a few tutorials for this part, some of their code is used in our project as of right now. The goal is to eventually do an overhaul/clean up of our code that makes it all self-written.

WiFi tutorial: https://www.teachmemicro.com/esp32-wifi-manager-dynamic-ssid-password

Firebase tutorial: https://randomnerdtutorials.com/esp32-firebase-realtime-database/

### What was done:
I first wanted to get WiFi working. I wanted to avoid hardcoding the SSID/Password, so first I needed to implement a way to have the user manually input the SSID/PW.

I found the WiFi tutorial linked above, which saves the SSID/PW into the EEPROM so it isn't lost between startups. When the ESP-32 boots up, it checks for a saved network configuration. If it is found, it connects to the WiFi network. Otherwise, it starts it's own network that we can connect to via a phone or computer. Going to 192.168.4.1 brings up a webpage where we can enter in the SSID/PW.

After WiFi started working, I followed the firebase tutorial to send and receive data to a firebase server. I wrote new functions that sets bools, floats, and ints in the firebase server, and tested it to ensure everything worked.

### Relavant Commit(s)
in firmware project: 8f6d95f999ffb673589b7599a1938df49b20f7b3

### Todo:
1. Contingency if network config is saved, but can't connect
Idea: flash all LEDs red when not connected to network, go into router mode.
2. Change the IP to maybe a URL? Not sure if possible
3. Add basic css to the website to make it look better.

## February 17 2022

### Objectives:
There were two things that was done: First was to meet with team members to check up on progress with the website, as well as update them with my progress on the firmware and schematics. We also wanted to figure out a way to store task info on the firebase server.

### What was done:
We started the meeting by going over the Schematic/Firmware work. Hongru then presented his progress on the website. Finally, we discussed firebase organization and how we will save the task names and statuses.

After the meeting, I worked on implementing that organization within the firmware. The first problem I had to tackle was giving the device a unique ID so we can have multiple devices connected to the server (at least in theory). This meant adding code to show the device ID after connecting to wifi, as well as adding the device ID to the firebase addresses. Formatting the string was somewhat convoluted, especially with Arduino's special String class, but I was able to figure it out after some experiementation. I also added a "addTask" function for testing purposes, as the esp32 won't need to do that when we are actually using the device. There is also an int that keeps track of how many tasks are in the database.

![image](images/firebaseLayout.JPG)

### Relatant Commit(s)
fc0a7390792a72708aa1fe9a42e2e179a4bb1b10

## February 21 2022

### Objectives:
The two main goals today is to have a mockup of the mechanical design for our device, as well as start on our Design Document.

### What was done:
I installed Fusion 360, dusted off my CAD ~~skills~~, and drafted a design of the device. I eyeballed everything except the screen size, and LED size. During this process, I switch from 3.5x3.5 LEDs to 5.0x5.0 LEDs because the former seemed too small.

![image](images/front_view.png)

![image](images/back_view.png)

After that, I helped my teammates work on our design document draft, mostly with the Module Requirements and Verification.

## February 21 2022

### Objective:
Design Doc Check

### What was done:
Presented design doc to Prof/TAs, received feedback

### Todo:
Get a CO2 sensor ASAP, prof had concerns over accuracy.

## March 1 2022

### Objective:
Design Review

### What was done:
Presented design to Prof. Song, He questioned alot about the radar sensor. We then went to the lab and tested the sensor with varying resistor values to make it less sensitive. We determined that without a resistor, it was able to detect minute movements from 1 meter away (we moved <1cm and it was able to detect it). However, it can detect large movements from far away >5m. We tested different resistor values and settled at 1.2 MOhm as a nice sensitivity for the sensor. We also determined that it was able to work through metal and plastic, and was omnidirectional.

## March 1 2022 - March 7 2022

### Objective:
Check PCB layout

### What was done
Worked with Pranav Goel in verifying his layout of the V1 PCB. Found some issues and was able to resolve them before the deadline.

## March 8 2022

### Objective:
Add a way to read tasks from firebase server

### What was done:
Added a bool in the server called update, when a task is added it gets set to true. The esp32 querys it every minute, and if it's true it gets the taskCount, iterates over all the tasks, and saves the task name and status to arrays. After all of that is done, it sets update to false. It also auto updates upon startup for the first time to pull whatever it needs to from the firebase server.

## March 10 2022:

### Objective
Create BOM and purchase parts

### what was done:
Worked with teammates to go through each individual component, and generate a spreadsheet that can be used to purchase the parts. It included the part name, quanitity needed, quantity required, link to part, Reference, Footprint, and if it was ordered and quanitity ordered. Some parts were out of stock, so we had to find alternatives. We also fixed some mistakes with the buck circuit layout where the wrong footprint was selected for the inductor and diode.

## March 24 2022 6:00 PM to 6:30 AM:

### Objective:
Meet with TA, then meet with team to discuss plans

### what was done
Met with Pooja from 6:00 to 6:15, discussed PCB deadlines, our order not going through, personal progress reports.

Met with Pranav and Hongru afterwards, discussed our next steps, how to fix our order not going through, and how to store sensor data in firebase.

Goal by Monday (March 28)
1. Me: Finish routing of PCB
2. Pranav: Get E-Ink display working with dev board
3. Hongru: Implement storing data on firebase using esp32
4. All: Solder V1 PCB provided parts arrive, try to test what we can

## March 24 2022 6:30 PM to 1:30 AM:

### Objective:
Create V2 PCB Layout

### what was done
The original plan was to start on our V2 board layout in order to finish before the order deadline. However, I ended up working from 6PM to 1AM, and finished all of the routing and layout. In order to do this, I started by placing the parts in logical places. Because the thing that mattered most was the placement of the buttons and encoder, I started by placing those in their respective places. Then, I put the esp32 in the back, near the edge so the antenna wasn't blocked by the PCB. Then, I moved the rest of the parts according to what was closest to their respective pins on the ESP32.

My goal was to leave the ground plane as unbroken as possible while minimizing size. In order to do this, I tried to place things so that long traces can be routed along the edge of the board.

After placing the parts in their starting position, I started routing the E-ink connector because that had the most pins. This was placed along the right edge of the board, so I was able to route it to the back using vias. Then, I wored on the JTAG connector, because it was close to the eink connector, and followed pretty much the exact same path. THen, I moved on to the power circuit, and worked on routing the 12V to 5V buck converter. This was slightly more difficult, since the diode and inductor were pretty big. However, with some help from the datasheet, I was able to finish that part. The 5V to 3V3 linear converter was trivial in comparision. 

Next, I routed the Sensing subsystem. The temperature and humidity sensor and the air quality sensor were both connected through the same I2C bus, so it was relativly trivial to run traces along the bottom to the respective pins on the ESP-32. I was a bit concerned about cross talk between the clock and data lines since they were placed close together, but upon doing further reasearch, running I2C at its normal speed shouldn't cause any problems. I also tried to move it away from the ESP-32 and power components in case heat from those components can affect the readings.

I then worked through the UI subsystem. The LEDs were pretty trivial to route, as the were on the empty back half of the PCB, and were individually addressable, so they were able to be daisy-chained together. The buttons and encoder were slightly harder to route, so I moved them around a bit until I was able to get traces running along the edge of the board to the ESP-32.

At the end, I realized I still had the UART connector left. I realized that I had painted myself into a corner, and had no way of getting the tx and rx lines connected in the room I had left. Therfore I ended up doing a major rework of the JTAG, E-ink, and UART connectors. By changing the eink and uart connector as well as moving the JTAG connector, I was able to afit everything and finish routing the PCB.

I then ran the DRC, and fixed the issues it raised. I also added/changed the mounting holes and made them smaller compared to V1 of our PCB.

In the process of doing the layout, I found a few mistakes with the layout and design of our first PCB. Firstly, the placement of the power connector was wrong. After fixing this, I realized that the buttons had the wrong pins assigned. Both of these were relativly simple to fix for V1, and should have simple workarounds for testing our first PCB.

### TODO:
1. Make grounding better, some parts of the ground plane are a bit narrow/divided
2. Round corners of PCB
3. Add logo/names/group
4. Fill front plane
5. Check silkscreen layer, change names, add labels, make sure orientation is correct
6. Generate 3d model

### Images:
Progress at 11PM
![image](images/layout11pm.PNG)

Final front and back layout (1:30AM)
![image](images/layoutfront.png)

![image](images/layoutback.png)

## March 24 2022

### Objective:
Complete the TODOs from yesterday, start soldering PCB.

### What was done
Breifly stopped by the lab to help teammates solder the ESP32 onto the PCB, then went home to do the things mentioned in the previous notebook entry. Moved wires around to make the back ground plane a bit more continuous, added a front ground plane and vias to connect the two. Also added logo and team info onto the front silkscreen, as well as better labels for the connectors and testpoints. Also rounded the corners of the PCB.

### Images:
![image](images/front_v2_1.png)
![image](images/back_v2_1.png)
![image](images/front3d.png)
![image](images/back3d.png)

## March 26 2022

### Objective:
Clean up the PCB

### What was done:
Moved stuff around, cleaned up the grounding, very minor changes to PCB.

## March 28 2022

### Objective:
Solder more components to the PCB, try flashing using a USB to Serial Converter

### What was done:
I used a USB to Serial converter to power and flash the ESP32. Flashing was successful, but the program did not run after flashing was finished. Apon further research, it turned out that the converter we had did not supply enough power to run the ESP-32. I plan to use the benchtop power supply in the future to power the ESP32. In order to put the bootloader into flash mode, I had to solder in the BOOT and EN button circuits. Instead of a button, I used jumpers and jumper wires on the PCB because it was wired wrong. Also swapped some pins around on the V2 PCB since pin 35 on the ESP-32 is input only.

## March 29 2022

### Objective:
Get ESP-32 working, test pins

### What was done:
Tested PCB. Tried multiple things including different combonations of BOOT and EN pins, holding pins 12/15 high/low, and providing more power via a benchtop power supply. Ended up figuring out with the help of Pranav Goel that two resistors were soldered wrong on the EN pin, causing it to be always held low. Once it was resoldered, we were able to flash with no issue. I then helped Pranav write a small Arduino program that blinked each output IO pin on and off, and he was able to verify all the EINK display pins worked.

Finally, I soldered on the rotary encoder and write a program to verify it worked.

## March 30 2022

### Objective:
Test I2C, input MAC address to enable wifi access with IllinoisNet_Guest, research OTA updates, fix firebase

### What was done:
I started by testing the I2C connections on our PCB. I found an Arduino sketch that looped through all the possible i2C addresses and looked for a response to find attached devices. Our sensors have not arrived yet, so I had to use the oscilloscope to verify the SCL and SDA lines were working, and that the voltages were in the allowable range.

SCL:
![image](images/scl.jpg)

SDA:
![image](images/sda.jpg)

I then moved on to trying to enable OTA updates. This was because as we develop further and add any type of enclosure, accessing the UART connector will become prohibitivly difficult. Adding the required code was pretty easy, I just followed a tutorial I found online. However, it took a while to actually get working. I tried many ways of debugging, including adding the device through IllinoisNet_Guest with its MAC address, fixing problems with the firebase server when it changed out of test mode over break, adding functionality to delete saved passwords, and more. In the end, I couldn't find a way around the schools wifi security features, and used my phone's hotspot to test OTA updates.


### Links:
https://lastminuteengineers.com/esp32-ota-updates-arduino-ide/

## March 30 2022 pt 2

### Objective:
Modify old CAD model to fit actual components

### What was done:
I modified the mock-up CAD to fit the actual PCBs and components using the dimensions in our layouts as well as datasheets for the components. I also sliced up the model to make it easier to 3d print.

![image](images/newCAD.jpg)

### Links
https://www.waveshare.com/w/upload/6/60/7.5inch_e-Paper_V2_Specification.pdf

## March 31 2022

### Objective:
Solder and test LED pcb

### What was done:
Soldered 2 RGB LED's to our second PCB for testing along with the data resistor and power filtering capacitors. Discovered back ground plane was disconnected from the front, where all the LEDs were. Soldered ground wire directly to the first LED ground pin, tested with simple arduino example code.

## April 1 2022 pt 1

### Objective:
develop firmware further, add in functionality to select tasks, add way to show task status and selected task on LEDS

### What was done
1. renamed einkManager.h to taskManager.h
2. added helper funtions in taskManager.h
3. Created UI.h to read rotary encoder inputs
4. Created LED.h to get taskstates and set LED colors
5. Upon testing, realized neopixel lib blocks other stuff from running
6. Moved encoder to an Interrupt triggered by rising edge - didn't work
7. Reworked LED.h to run as minimally as possible
8. Added function to increment firebase task state;
9. Tested all of above, fixed bugs, all but highlighting selected task works (rotary encoder changes selected task, pressing integrated button advances task state in both LED color and in the firebase server)
10. Changed to FastLED library since it offers better performance from home, will test in lab tomorrow


## April 1 2022 pt 2

### Objective: 
Find a way around running out of program space on esp32

### What was done:
While debugging the stuff done in part 1, I noticed that we were using 87% of program space. That was a problem because the E-ink code used 27% seperately, and we haven't even gotten to the sensor reading code yet. Therefore, I had to find a way to use less storage or get more. I ended up pinpointing the source to the Firebase library, since my firebase test code itsself took up 80%. First I tried experiementing with MQTT and AWS services, but that was pretty complicated, and would have required a complete overhaul of almost all the programming we've done up to this point. I also briefly explored several options, but by far the best one was to change the partition tables on the esp32 to give more room to the application section. However, in order to do so, I had to learn how to use platformio. It was relativly simple to set up, and honestly much much much better than switching between vscode and arduino. It allows us to put needed librares in the code itsself, is much more similar to base C++, and offers a lot more freedom. It is also natively in VSCode, which makes my workflow simpler.

### Commit
9f92bf73880ecc853b2bd2a9db97721d8b299e53

### Links
https://randomnerdtutorials.com/vs-code-platformio-ide-esp32-esp8266-arduino/
https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/partition-tables.html

## April 2 2022

### Objective: Test new updates (change to platformIO/partition table, FastLED)

### What was done:
Tested partition table, wifi credentials were still saved as usual. However, FastLEDs did not support RGBW LEDs. I found a tutorial that showed how to add an extra bit per LED to enable rgbw leds. Also converted the colors to HSV to make dimming each LED easier. The FastLED library also works much faster, so the rotary encoder also works much better.

Also, because I had no idea how to use the Arduino IDE, all of my code was in .h files and were all in-line. Because I moved to platformIO, I was able to convert the .h files into actual .h and .cpp files, making the code neater and easier to read.

### Commits
34f04030537f25d36bb1e3928e45d2469d300467
afac4831bee15fbdda17cfb95985235597744a29

### Links
https://www.partsnotincluded.com/fastled-rgbw-neopixels-sk6812/

### TODO:
Make leds all dim after inactivity
troubleshoot wifi connection problem

## April 12 2022

### Objective: Solder and test pcb

### What was done:
Got V2 PCB, soldered 12V -> 5V circuit, 5V-3V3 circuit, and esp32 circuit to make sure all was working. Flashed a simple program to verify everything worked on the microcontroller side.

## April 13 2022

### Objective: Solder on sensors

### What was done:
The sensors we chose was really small (SHT40 is 1.5x1.5 mm with 4 pins), so I leanred how to use the hot air gun and solder paste. It took a few practice runs and a few tries but we were able to get both of them soldered on.

## April 14 2022:

### Objective: Solder on rest of stuff on PCB, test current code

### What was done:
I soldered on the last 2 rgb leds on the pcb, and tested the new code on it. Ran into a bug where it ended up being caused by me not setting up the new device on illinoisnet_guest. Also had to debug the led circuit because the soldering wasn't the best and some pins were not making contact. Also got rid of the anonymous firebase access, set up email access from bx3@illinois.edu.

Was on a roll so continued by testing sensors, found libraries for both SHT40 and SGP40, tested both of them and both returned data. Wrote and added sensor module to project.

## April 18 2022:

### Objective: OTA stuff, Fix design issues

### what was done:
Worked on the C side of the OTA, added script to delete main.cpp.o file so __TIME__ and __DATE__ always update, fixed design issues on the front plate to hopefully fit our pcb and display

## April 19 2022:

### Objective: Implement multicore

### What was done
Worried about eink display and internet stuff blocking the UI stuff, such as the encoder and LEDs. Wanted to implement multicore using freeRTOS. Split into two cores, 

Task 1 (Core 1): OTA, WIFI, Firebase, e-ink
Task 2 (Core 0): Sensors, LEDs, Button/Encoders, motion, taskmanager

After extensive debugging, discovered OTA only worked on core 1, and required core 0 to be not doing anything so I added in a mutex to check that. Also, nothing on core 0 can access core 1 variables, and vice versa, so had to add queues to make sensors update the firebase with data. Might need to do same with motion sensor.
